auditionrush-modules
====================

Modules for Auditionrush Titanium App

These module are already unzipped and placed in the modules directory.

Info on installing: https://wiki.appcelerator.org/display/tis/Using+Titanium+Modules

Copy the modules:

com.0x82.sharekit
com.0x82.key.chain

to this location:

~/Library/Application Support/Titanium/modules/iphone



The end results should be:

~/Library/Application Support/Titanium/modules/iphone/com.0x82.sharekit
 
~/Library/Application Support/Titanium/modules/iphone/com.0x82.key.chain 



